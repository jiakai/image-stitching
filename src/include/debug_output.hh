/*
 * $File: debug_output.hh
 * $Date: Tue Apr 30 17:53:45 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "math.hh"
#include "vector.hh"
#include <iostream>
#include <vector>

template<typename T>
std::ostream& operator << (std::ostream &ostr, const std::vector<T> &v) {
	bool first = true;
	for (auto &x: v){
		if (first)
			first = false;
		else {
			ostr << ' ';
		}
		ostr << x;
	}
	return ostr;
}

template<typename T>
std::ostream& operator << (std::ostream &ostr, const Matrix<T> &v) {
	for (int i = 0; i < v.height(); i ++) {
		bool first = true;
		for (int j = 0; j < v.width(); j ++) {
			if (first)
				first = false;
			else
				ostr << ' ';
			ostr << v.at(i, j);
		}
		ostr << endl;
	}
	return ostr;
}

static inline std::ostream& operator <<
		(std::ostream &ostr, const Vector2D &v) {
	return ostr << "(" << v.x << "," << v.y << ")";
}

static inline std::ostream& operator <<
		(std::ostream &ostr, const Vector3D &v) {
	return ostr << "(" << v.x << "," << v.y << "," << v.z << ")";
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

