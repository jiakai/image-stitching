/*
 * $File: imgio.hh
 * $Date: Tue Apr 09 19:45:59 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

/*!
 * \file
 * \brief Image I/O interface
 */

#include "utils.hh"
#include "image.hh"


/*!
 * \brief image file reader
 */
class ImageReader {
	int m_width, m_height;
	std::shared_ptr<pixel_t> m_color_channel[3], m_gray;

	public:
		ImageReader(const char *fpath = nullptr);
		void read(const char *fpath);

		/*!
		 * \brief get the image of a specified channel
		 * \param ch channel number, 0 to 2 corresponding to r, g, b
		 */
		Image get_channel(int ch) const {
			assert(ch >= 0 && ch < 3);
			return Image(m_width, m_height, m_color_channel[ch]);
		}

		Image get_gray() const
		{ return Image(m_width, m_height, m_gray); }
};

/*!
 * \brief write an image
 */
void save_image(const char *fpath,
		const Image &ch_r, const Image &ch_g, const Image &ch_b);

static inline void save_image(const char *fpath, const Image &ch_gray) {
	save_image(fpath, ch_gray, ch_gray, ch_gray);
}


/*!
 * \brief display an image on screen
 */
void display_image(const Image &ch_r, const Image &ch_g, const Image &ch_b);

static inline void display_image(const Image &ch_gray) {
	display_image(ch_gray, ch_gray, ch_gray);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

