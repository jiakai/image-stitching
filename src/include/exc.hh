/*
 * $File: exc.hh
 * $Date: Tue Apr 02 19:41:54 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include <exception>
#include <string>

class IstitcherError: public std::exception {
	std::string m_msg;

	public:
		IstitcherError(const char *fmt, ...)
			__attribute__((format(printf, 2, 3)));

		~IstitcherError() throw () {}

		const char *what() const throw()
		{ return m_msg.c_str(); }
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

