/*
 * $File: linear.hh
 * $Date: Tue Apr 23 16:53:57 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "base.hh"
#include <vector>

class LinearBlender: public BlenderBase {
	struct Range2D {
		int xmin, xmax, ymin, ymax;
		Range2D(const Point &upper_left, int width, int height):
			xmin(upper_left.r), xmax(xmin + height),
			ymin(upper_left.c), ymax(ymin + width)
		{}

		inline bool contain(int x, int y) const {
			return x >= xmin && x < xmax && y >= ymin && y < ymax;
		}
	};
	struct WeightedPixel {
		pixel_t intensity;
		real_t weight;
	};
	struct ImageComponent {
		Matrix<WeightedPixel> img;
		Range2D range;
		ImageComponent(const Matrix<WeightedPixel> &i, const Range2D &r):
			img(i), range(r)
		{}
	};
	std::vector<ImageComponent> m_image;

	public:
		void add_image(const Point &upper_left,
				const Matrix<Vector2D> &orig_pos, const Image &img);

		void run(Image &target);
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

