/*
 * $File: fast_gaussian.hh
 * $Date: Sat Apr 27 17:27:58 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "base.hh"

#include <memory>
#include <cstring>

/*!
 * \brief Gaussian blur filter
 */
class FastGaussianFilter: public FilterBase {

	real_t m_sigma;

	/*!
	 * \brief use avg filter to filter a row
	 * \param cur_line the target row
	 * \param old_val buffer to store old values
	 * \param size filter window size
	 */
	template<typename scalar_t>
	inline void filter_row(scalar_t *cur_line, scalar_t *old_val,
			int size, int line_width,
			real_t center_coeff, real_t avg_coeff);

	public:
		using FilterBase::run;
		int m_nr_iter;	//! number of iterations

		FastGaussianFilter(real_t sigma);

		/*!
		 * note that scalar_t must be POD supporting memset and memcpy
		 * operators for scalar_t: +=, -=, +, *
		 * default constructor for scalar_t must creates 0
		 */
		template<typename scalar_t>
		void run(const Matrix<scalar_t> &input, scalar_t *output);

		void run(const Image &img_in, pixel_t *img_out) {
			return run<pixel_t>(img_in, img_out);
		}


		real_t sigma() const {
			return m_sigma;
		}

		void sigma(real_t sigma) {
			m_sigma = sigma;
		}
};

template<typename scalar_t>
void FastGaussianFilter::filter_row(
		scalar_t *cur_line, scalar_t *old_val, int line_width,
		int size, real_t center_coeff, real_t avg_coeff) {

	for (int iter = 0; iter < m_nr_iter; iter ++) { 
		scalar_t sum = scalar_t(); 
		for (int i = -size; i < size; i ++) 
			sum += (old_val[i] = cur_line[i]); 
		for (int i = 0; i < line_width; i ++) { 
			sum += (old_val[i + size] = cur_line[i + size]); 
			cur_line[i] = sum * avg_coeff + cur_line[i] * center_coeff; 
			sum -= old_val[i - size]; 
		} 
	} 

}

template<typename scalar_t>
void FastGaussianFilter::run(const Matrix<scalar_t> &input, scalar_t *output) {
	int height = input.height(), width = input.width(),
		size = 0; // half size of the moving window (range [-size, size])

	real_t center_coeff, avg_coeff;
	// val = avg_coeff * sum + center_coeff * window[0]
	{
		real_t filter_var = m_sigma * m_sigma / m_nr_iter;
		int sqr_sum = 0;
		real_t prob;
		for (; ;) {
			size += 1;
			sqr_sum += size * size * 2;
			prob = filter_var / sqr_sum;
			if (prob * size * 2 + 0.1 < 1)
				break;
		}
		avg_coeff = prob;
		center_coeff = 1 - prob * (size * 2) - prob;
	}

	int line_width = size * 2 + max(width, height);
	auto cur_line_mem = create_auto_buf<scalar_t>(line_width * 2);

	scalar_t *cur_line = cur_line_mem.get() + size,
			 *old_val = cur_line + line_width;

	// apply to columns
	for (int j = 0; j < width; j ++) {

		// copy value to cur_line
		{
			const scalar_t *src = input.data(0, j);
			for (int i = 0; i < height; i ++) {
				cur_line[i] = *src;
				src += width;
			}
		}

		// extend cur_line
		{
			scalar_t v0 = cur_line[0];
			for (int i = 1; i <= size; i ++)
				cur_line[-i] = v0;
			v0 = cur_line[height - 1];
			for (int i = 0; i < size; i ++)
				cur_line[height + i] = v0;
		}

		filter_row(cur_line, old_val, height, size, center_coeff, avg_coeff);

		// copy back to output
		{
			scalar_t *dest = output + j;
			for (int i = 0; i < height; i ++) {
				*dest = cur_line[i];
				dest += width;
			}
		}
	}

	// apply to rows
	for (int i = 0; i < height; i ++) {

		scalar_t *dest = output + i * width;
		memcpy(cur_line, dest, sizeof(scalar_t) * width);

		// extend cur_line
		{
			scalar_t v0 = cur_line[0];
			for (int j = 1; j <= size; j ++)
				cur_line[-j] = v0;
			v0 = cur_line[width - 1];
			for (int j = 0; j < size; j ++)
				cur_line[width + j] = v0;
		}

		filter_row(cur_line, old_val, width, size, center_coeff, avg_coeff);
		memcpy(dest, cur_line, sizeof(scalar_t) * width);
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

