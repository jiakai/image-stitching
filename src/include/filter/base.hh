/*
 * $File: base.hh
 * $Date: Thu Apr 04 22:06:43 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "image.hh"

class FilterBase {
	public:
		virtual ~FilterBase() {}
		virtual void run(const Image &img_in, pixel_t *img_out) = 0;

		void run(const Image &img_in, Image &img_out) {
			img_out.set_dimension(img_in);
			run(img_in, img_out.data());
		}

		/*!
		 * \brief apply the filter on a newly created image
		 */
		virtual Image run(const Image &img_in) {
			Image ret;
			run(img_in, ret);
			return ret;
		}
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

