/*
 * $File: equalizer.hh
 * $Date: Sat Apr 27 13:21:52 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "base.hh"

/*!
 * \brief trainable equalizer that makes color distribution of an image look
 *		like that of another
 */
class TrainableEqualizerFilter: public FilterBase {
	class Impl;
	Impl *m_impl;

	TrainableEqualizerFilter(const TrainableEqualizerFilter &) = delete;
	TrainableEqualizerFilter& operator = (const TrainableEqualizerFilter &)
		= delete;

	public:
		using FilterBase::run;

		int m_nr_quantum;	/*! number of intervals to split pixel value [0, 1]
							  into */
		TrainableEqualizerFilter();
		~TrainableEqualizerFilter();

		/*!
		 * \brief add a training case, telling that src should be translated to
		 *		dest
		 */
		void add(pixel_t src, pixel_t dest);

		/*!
		 * \brief finish adding training cases
		 */
		void finish_add();

		void run(const Image &img_in, pixel_t *img_out);

		void run_modify(Image &img);
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

