/*
 * $File: config.hh
 * $Date: Sat Apr 27 10:16:57 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "mathbase.hh"
#include <vector>

class Config {
	class KVMap;
	KVMap *m_map;
	
	public:
		Config(const char *fpath);
		~Config();

		int get_int(const char *key, int default_val);
		double get_double(const char *key, double default_val);
		std::vector<real_t> get_real_t_vector(const char *key,
				const std::vector<real_t> &default_val);
};

extern Config config;	// defined in main

#define DEBUG_DIR	"data/debug/"

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

