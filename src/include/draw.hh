/*
 * $File: draw.hh
 * $Date: Thu Apr 18 20:50:43 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

/*!
 * \file
 * \brief drawing objects on images
 */

#include "image.hh"
#include "vector.hh"

namespace draw {
	void point(Image &img, const Point &pos, pixel_t color = 1);
	void circle(Image &img, const Point &center, int radius, pixel_t color = 1);
	void line(Image &img, const Point &p0, const Point &p1, pixel_t color = 1);
	void vector(Image &img, const Point &p0, const Point &p1,
			pixel_t color = 1);
	/*!
	 * \brief put src on dest, with upper-left corner positioned at pos
	 */
	void image(Image &dest, const Point &pos, const Image &src);

	/*!
	 * \brief scaled coordinate (range 0 to 1)
	 */
	inline void line_scaled(Image &img, const Vector2D &p0, const Vector2D &p1,
			pixel_t color = 1) {
		line(img,
				Point(ifloor(p0.x * img.height()), ifloor(p0.y * img.width())),
				Point(ifloor(p1.x * img.height()), ifloor(p1.y * img.width())),
				color);
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

