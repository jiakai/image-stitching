/*
 * $File: math.hh
 * $Date: Thu May 02 14:30:08 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "utils.hh"
#include "mathbase.hh"

#include <cmath>

template<typename T>
T sqr(const T &v) {
	return v * v;
}

template <typename T>
static inline T dot_product(const T *a, const T *b, size_t len) {
	T s = 0;
	for (size_t i = 0; i < len; i ++)
		s += a[i] * b[i];
	return s;
}

template <typename T>
static inline T calc_length(const T *a, size_t len) {
	T s = 0;
	for (size_t i = 0; i < len; i ++)
		s += a[i] * a[i];
	return sqrt(s);
}

template <typename T>
static inline T calc_euclidean_dist(const T *a, const T *b, size_t len) {
	T s = 0;
	for (size_t i = 0; i < len; i ++)
		s += sqr(a[i] - b[i]);
	return sqrt(s);
}

template <typename T>
inline T quadratic_sum(T v0) {
	return v0 * v0;
}

template <typename T, typename... Args>
inline T quadratic_sum(T v0, Args... args) {
	return v0 * v0 + quadratic_sum(args...);
}

template <typename T, typename... Args>
inline T quadratic_sum_sqrt(T v0, Args... args) {
	return sqrt(quadratic_sum(v0, args...));
}

template <typename T>
inline T min(T v0) {
	return v0;
}

template <typename T, typename... Args>
inline T min(T v0, Args... args) {
	T tmp = min(args...);
	if (v0 < tmp)
		return v0;
	return tmp;
}

template <typename T>
inline T max(T v0) {
	return v0;
}

template <typename T, typename... Args>
inline T max(T v0, Args... args) {
	T tmp = max(args...);
	if (v0 > tmp)
		return v0;
	return tmp;
}

template <typename T>
int iceil(T x) {
	return int(ceil(x));
}

template <typename T>
int ifloor(T x) {
	return int(floor(x));
}

template <typename T>
inline T& update_min(T &target) {
	return target;
}

template <typename T, typename... Args>
inline T& update_min(T &target, const T &val, Args... args) {
	if (val < target)
		target = val;
	update_min(target, args...);
	return target;
}

template <typename T>
inline T& update_max(T &target) {
	return target;
}

template <typename T, typename... Args>
inline T& update_max(T &target, const T &val, Args... args) {
	if (val > target)
		target = val;
	update_max(target, args...);
	return target;
}

/*!
 * \brief return -1, 0, 1 if x < 0, x == 0, x > 0
 */
static inline int get_sign(real_t x) {
	if (x < -EPS)
		return -1;
	if (x > EPS)
		return 1;
	return 0;
}

template<typename scalar_t>
class Matrix {
#define CHECK_RANGE(r, c) assert(r >= 0 && r < m_height && \
		c >= 0 && c < m_width)

	int m_allocated_size;
	protected:
		int m_width, m_height;
		std::shared_ptr<scalar_t> m_data;

	public:
		Matrix():
			m_allocated_size(0), m_width(0), m_height(0)
		{}

		Matrix(int w, int h, std::shared_ptr<scalar_t> data):
			m_allocated_size(w * h), m_width(w), m_height(h), m_data(data)
		{}

		int width() const { 
			return m_width;
		}

		int height() const {
			return m_height;
		}

		int area() const {
			return m_width * m_height;
		}

		/*!
		 * \brief return a pointer to the first element
		 */
		const scalar_t* data() const
		{ return m_data.get(); }

		/*!
		 * \brief return a pointer to the first element
		 */
		scalar_t* data()
		{ return m_data.get(); }

		/*!
		 * \brief return a pointer to the element at r'th row, c'th col
		 */
		const scalar_t* data(int r, int c) const {
			CHECK_RANGE(r, c);
			return m_data.get() + r * m_width + c;
		}

		/*!
		 * \brief return a pointer to the element at r'th row, c'th col
		 */
		scalar_t* data(int r, int c) {
			CHECK_RANGE(r, c);
			return m_data.get() + r * m_width + c;
		}


		/*!
		 * \brief set the dimension of this matrix (allocate new buf if
		 *		necessary)
		 */
		Matrix& set_dimension(int width, int height) {
			if (width * height > m_allocated_size)
				reset(width, height);
			else
			{
				m_width = width;
				m_height = height;
			}
			return *this;
		}

		/*!
		 * \brief set the dimension of this matrix (allocate new buf if
		 *		necessary)
		 */
		template <typename T>
		Matrix& set_dimension(const Matrix<T> &m) {
			return set_dimension(m.m_width, m.m_height);
		}

		/*!
		 * \brief allocate a new buffer filled with 0 for this matrix
		 */
		Matrix& reset(int width, int height) {
			m_width = width;
			m_height = height;
			m_allocated_size = width * height;
			m_data = create_auto_buf<scalar_t>(width * height, true);
			return *this;
		}

		/*!
		 * \brief return the element at a specific location
		 */
		const scalar_t& at(int r, int c) const {
			CHECK_RANGE(r, c);
			return m_data.get()[r * m_width + c];
		}

		/*!
		 * \brief return the element at a specific location
		 */
		scalar_t& at(int r, int c) {
			CHECK_RANGE(r, c);
			return m_data.get()[r * m_width + c];
		}

		/*!
		 * \brief return the element at a specific location; if out of boundary,
		 *		return the default value
		 */
		scalar_t at(int r, int c, scalar_t default_) const {
			if (r < 0 || r >= m_height || c < 0 || c >= m_width)
				return default_;
			return m_data.get()[r * m_width + c];
		}

		/*!
		 * \brief return the element at a specific location; if out of boundary,
		 *		return the default value
		 */
		scalar_t* at(int r, int c, scalar_t *default_) const {
			if (r < 0 || r >= m_height || c < 0 || c >= m_width)
				return default_;
			return m_data.get() + r * m_width + c;
		}

		/*!
		 * \brief fill the matrix using memset
		 */
		void fill_memset(int val) {
			memset(m_data.get(), val, m_width * m_height * sizeof(scalar_t));
		}

		scalar_t* begin() {
			return m_data.get();
		}

		scalar_t* end() {
			return m_data.get() + m_width * m_height;
		}

		const scalar_t* begin() const {
			return m_data.get();
		}

		const scalar_t* end() const {
			return m_data.get() + m_width * m_height;
		}

		Matrix<scalar_t> copy() const {
			Matrix<scalar_t> ret(m_width, m_height,
					create_auto_buf<scalar_t>(m_width * m_height));
			memcpy(ret.m_data.get(), m_data.get(),
					sizeof(scalar_t) * m_width * m_height);
			return ret;
		}
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

