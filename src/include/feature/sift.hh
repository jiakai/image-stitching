/*
 * $File: sift.hh
 * $Date: Sun Apr 28 22:17:51 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

/*!
 * \file
 * \brief Scale Invariant Feature Transform by Lowe
 */

#include "base.hh"
#include "math.hh"

class SIFTFeature: FeatureBase {
	class Impl;
	Impl *m_impl;

	SIFTFeature(const SIFTFeature &) = delete;
	SIFTFeature& operator = (const SIFTFeature &) = delete;

	public:
		// config options, could be changed on the fly
		int m_nr_octave, m_nr_dog_per_octave,
			m_border_size,	//! ignore pixels within this region near border
			m_orientation_nr_bin,
			m_subpxlrefine_nr_iter,	/*! maximal number of iterations for
									  subpixel extrema */
			m_descr_hist_win_width,	//! width of each histogram window 
			m_descr_hist_nr_bin,
			m_descr_width,			//! number of histograms horizontally
			m_max_image_size;		//! downsample large image to this size
		pixel_t m_contrast_threshold;
		real_t m_preblur,	//! preblur the image before processing
			   m_init_sigma,	//! assumed sigma of the input image
			   m_sigma_start, m_sigma_end,	//! sigma of layers in each octave
			   m_edge_threshold,
			   m_orientation_sigma, m_orientation_radius,
			   m_orientation_peak_ratio,
			   m_descr_len_thr;	//! threshold for descriptor magnitude

		SIFTFeature(const Image &img);
		~SIFTFeature();

		std::vector<Point> detect();
		FeatureKeypointArray extract_feature();

		/*!
		 * \brief draw keypoint on a copy of the original image
		 */
		Image draw_keypoint();
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

