/*
 * $File: base.hh
 * $Date: Mon Apr 29 10:00:22 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "image.hh"

#include <vector>

struct FeatureKeypoint {
	int id;	//! each keypoint has a unique id (used for comparation)
	real_t r, c;	//! ranges from 0 to 1
	std::vector<real_t> descriptor;

	FeatureKeypoint(int r_, int c_, const std::vector<real_t> &desc):
		id(next_id()), r(r_), c(c_), descriptor(desc)
	{}

	FeatureKeypoint():
		id(next_id()), r(-1), c(-1)
	{}

	private:
		static int next_id() {
			static int id;
			return __sync_fetch_and_add(&id, 1);
		}
};

using FeatureKeypointArray = std::vector<FeatureKeypoint>;

class FeatureBase {
	protected:
		Image m_image;

	public:
		FeatureBase(const Image &img):
			m_image(img)
		{}

		virtual ~FeatureBase() {}

		/*!
		 * \brief detect and return basic keypoints
		 */
		virtual std::vector<Point> detect() = 0;

		/*!
		 * \brief extract features at each keypoint
		 */
		virtual FeatureKeypointArray extract_feature() = 0;
};


class FeatureMatcherBase {
	protected:
		FeatureKeypointArray m_feature_set;

	public:
		FeatureMatcherBase() {}

		FeatureMatcherBase(const FeatureKeypointArray &f):
			m_feature_set(f)
		{}

		virtual ~FeatureMatcherBase()
		{}

		virtual void add_feature(const FeatureKeypoint &f) {
			m_feature_set.push_back(f);
		}

		virtual void add_feature(const FeatureKeypointArray &array) {
			m_feature_set.insert(m_feature_set.end(),
					array.begin(), array.end());
		}

		/*!
		 * \brief try to match the feature, return pointer to matched feature,
		 *		or nullptr if no match found
		 * \param dist the distance of matched feature
		 */
		virtual const FeatureKeypoint* match(
				const FeatureKeypoint &key, real_t *dist = nullptr) const = 0;

		/*!
		 * \brief return the underlying feature set
		 */
		const FeatureKeypointArray& get_feature_set() const {
			return m_feature_set;
		}

};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

