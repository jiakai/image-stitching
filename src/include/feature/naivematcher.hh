/*
 * $File: naivematcher.hh
 * $Date: Mon Apr 29 10:00:36 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "base.hh"

class NaiveFeatureMatcher: public FeatureMatcherBase {
	public:
		real_t m_ratio_threshold;	/*! threshold for ratio between nearest and
										second-nearest match */

		NaiveFeatureMatcher(const FeatureKeypointArray
				&feature_set = FeatureKeypointArray());

		const FeatureKeypoint* match(const FeatureKeypoint &key,
				real_t *dist = nullptr) const;
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

