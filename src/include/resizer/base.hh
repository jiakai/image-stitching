/*
 * $File: base.hh
 * $Date: Fri Apr 05 16:02:50 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

/*!
 * \file
 * \brief resampling images
 */

#include "image.hh"
#include "math.hh"

class ResizerBase {
	public:
		virtual ~ResizerBase() {}

		/*!
		 * \brief resize the image (factor = new image size / orig image size)
		 */
		virtual Image resize(const Image &img_in, real_t factor) = 0;
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

