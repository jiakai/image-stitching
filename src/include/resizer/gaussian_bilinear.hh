/*
 * $File: gaussian_bilinear.hh
 * $Date: Sun Apr 28 21:43:38 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "base.hh"

/*!
 * \brief perform gaussian blur and then bilinear resize
 */
class GaussianBilinearResizer: public ResizerBase {
	public:
		Image resize(const Image &img_in, real_t factor);
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

