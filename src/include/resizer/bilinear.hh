/*
 * $File: bilinear.hh
 * $Date: Wed May 01 14:33:07 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "base.hh"

class BilinearResizer: public ResizerBase {
	void downsample_half(const Image &img_in, Image &img_out);

	public:
		bool use_half_downsample = true;
		Image resize(const Image &img_in, real_t factor);
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

