/*
 * $File: main.cc
 * $Date: Thu May 02 00:36:15 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "config.hh"
#include "imgio.hh"
#include "stitcher.hh"
#include "utils.hh"
#include "exc.hh"
#include "vector.hh"
#include "feature/sift.hh"
#include "feature/naivematcher.hh"

#include <cstring>
#include <cstdio>
#include <vector>
using namespace std;

Config config("config.txt");


class ImageWarpper {
	real_t m_z,
		   m_phi_range, m_u_range;
	int m_w, m_h;
	public:
		ImageWarpper(int width, int height):
			m_z(width),
			m_phi_range(atan2(width * 0.5, m_z) * 2),
			m_u_range(height / m_z),
			m_w(width), m_h(height)
		{}

		ImageWarpper(const Image &img):
			ImageWarpper(img.width(), img.height())
		{}

		Image warp(const Image &img) const {
			int w = img.width(), h = img.height();
			assert(w == m_w && h == m_h);
			Image rst = Image::create(w, h);
			for (int i = 0; i < h; i ++) {
				real_t u = (real_t(i) / h - 0.5) * m_u_range;
				for (int j = 0; j < w; j ++) {
					real_t phi = (real_t(j) / w - 0.5) * m_phi_range,
						   y = tan(phi) * m_z,
						   x = sqrt(y * y + m_z * m_z) * u;
					y += w * 0.5;
					x += h * 0.5;
					if (x >= 0 && x < h && y >= 0 && y < w)
						rst.at(i, j) = img.get_linear_interpolation(x, y);
							
				}
			}
			return rst;
		}

		Vector2D warp_point_sc(const Vector2D &v) const {
			assert(v.x >= 0 && v.x < 1 && v.y >= 0 && v.y < 1);
			real_t x = (v.x - 0.5 ) * m_h,
				   y = (v.y - 0.5) * m_w,
				   u = x / quadratic_sum_sqrt(y, m_z),
				   phi = atan2(y, m_z);
			return Vector2D(u / m_u_range + 0.5, phi / m_phi_range + 0.5);
		}
};

static void get_image_and_feature(const char *fpath, ImageReader &reader,
		FeatureMatcherBase &feature_mather);

int main(int argc, char **argv) {
	InitRegister::init();
	if (argc < 3) {
		fprintf(stderr, "usage: %s <output base name> images ...\n", argv[0]);
		return -1;
	}
	if (!confirm_overwrite(argv[1]))
		return 0;
	Timer __timer__([](Timer&t){
			log_printf("cpu time: %.3f[sec]  peak vm: %.3f[MB]",
				t.get_time(), get_peak_vm() / 1024.0 / 1024.0);
			});
	vector<ImageReader> img_reader(argc - 2);
	vector<NaiveFeatureMatcher> matcher(argc - 2);
	ImageStitcher::FeatureMatcherPtrArray matcher_ptr(argc - 2);

#pragma omp parallel for schedule(dynamic)
	for (int i = 2; i < argc; i ++) {
		get_image_and_feature(argv[i], img_reader[i - 2], matcher[i - 2]);
		matcher_ptr[i - 2] = &matcher[i - 2];
	}

	ImageStitcher stitcher;
	vector<Image> feature_image;
	for (auto &i: img_reader) {
		ImageWarpper w(i.get_gray());
		feature_image.push_back(w.warp(i.get_gray()));
	}

	// stitcher.m_draw_match = true;
	// stitcher.m_dump_homo_point = true;
	// stitcher.m_save_transformed_img = true;
	stitcher.init(matcher_ptr, feature_image);

	vector<Image> result[3]; // result for each channel
#pragma omp parallel for schedule(dynamic)
	for (int i = 0; i < 3; i ++) {
		vector<Image> img;
		for (auto &j: img_reader) {
			Image m = j.get_channel(i);
			ImageWarpper w(m);
			m = w.warp(m);
			img.push_back(m);
		}
		result[i] = stitcher.stitch(img);
	}

	for (size_t i = 0; i < result[0].size(); i ++) {
		log_printf("saving result %d", int(i));
		const Image &r = result[0][i], &g = result[1][i], &b = result[2][i];
		char fname[255];
		strncpy(fname, argv[1], sizeof(fname));
		if (i) {
			char ext[255], *p = fname + strlen(fname);
			for (; ; p --) {
				if (p < fname)
					throw IstitcherError("no filename ext found");
				if (*p == '.') {
					strcpy(ext, p);
					break;
				}
			}
			snprintf(p, sizeof(fname) - (p - fname), "%d%s", int(i), ext);
		}
		save_image(fname, r, g, b);
		// display_image(r, g, b);
	}
}

void get_image_and_feature(const char *fpath, ImageReader &reader,
		FeatureMatcherBase &feature_mather) {

	log_printf("reading image: %s", fpath);

	reader.read(fpath);
	SIFTFeature sift(reader.get_gray());
	ImageWarpper warper(reader.get_gray());
	sift.detect();
	// sift.draw_keypoint().display();
	auto ft = sift.extract_feature();
	for (auto &i: ft) {
		Vector2D p(i.r, i.c);
		p = warper.warp_point_sc(p);
		assert(p.x >= 0 && p.x < 1 && p.y >= 0 && p.y < 1);
		i.r = p.x;
		i.c = p.y;
	}
	feature_mather.add_feature(ft);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

