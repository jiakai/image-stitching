/*
 * $File: main.cc
 * $Date: Tue Apr 30 23:43:44 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "config.hh"
#include "imgio.hh"
#include "stitcher.hh"
#include "utils.hh"
#include "exc.hh"
#include "feature/sift.hh"
#include "feature/naivematcher.hh"

#include <cstring>
#include <cstdio>
#include <vector>
using namespace std;

Config config("config.txt");


static void get_image_and_feature(const char *fpath, ImageReader &reader,
		FeatureMatcherBase &feature_mather);

int main(int argc, char **argv) {
	InitRegister::init();
	if (argc < 3) {
		fprintf(stderr, "usage: %s <output base name> images ...\n", argv[0]);
		return -1;
	}
	if (!confirm_overwrite(argv[1]))
		return 0;
	Timer __timer__([](Timer&t){
			log_printf("cpu time: %.3f[sec]  peak vm: %.3f[MB]",
				t.get_time(), get_peak_vm() / 1024.0 / 1024.0);
			});
	vector<ImageReader> img_reader(argc - 2);
	vector<NaiveFeatureMatcher> matcher(argc - 2);
	ImageStitcher::FeatureMatcherPtrArray matcher_ptr(argc - 2);

#pragma omp parallel for schedule(dynamic)
	for (int i = 2; i < argc; i ++) {
		get_image_and_feature(argv[i], img_reader[i - 2], matcher[i - 2]);
		matcher_ptr[i - 2] = &matcher[i - 2];
	}

	ImageStitcher stitcher;
	vector<Image> feature_image;
	for (auto &i: img_reader)
		feature_image.push_back(i.get_gray());

	// stitcher.m_draw_match = true;
	// stitcher.m_dump_homo_point = true;
	// stitcher.m_save_transformed_img = true;
	stitcher.init(matcher_ptr, feature_image);

	vector<Image> result[3]; // result for each channel
#pragma omp parallel for schedule(dynamic)
	for (int i = 0; i < 3; i ++) {
		vector<Image> img;
		for (auto &j: img_reader)
			img.push_back(j.get_channel(i));
		result[i] = stitcher.stitch(img);
	}

	for (size_t i = 0; i < result[0].size(); i ++) {
		log_printf("saving result %d", int(i));
		const Image &r = result[0][i], &g = result[1][i], &b = result[2][i];
		char fname[255];
		strncpy(fname, argv[1], sizeof(fname));
		if (i) {
			char ext[255], *p = fname + strlen(fname);
			for (; ; p --) {
				if (p < fname)
					throw IstitcherError("no filename ext found");
				if (*p == '.') {
					strcpy(ext, p);
					break;
				}
			}
			snprintf(p, sizeof(fname) - (p - fname), "%d%s", int(i), ext);
		}
		save_image(fname, r, g, b);
		// display_image(r, g, b);
	}
}

void get_image_and_feature(const char *fpath, ImageReader &reader,
		FeatureMatcherBase &feature_mather) {

	log_printf("reading image: %s", fpath);

	reader.read(fpath);
	SIFTFeature sift(reader.get_gray());
	sift.detect();
	// sift.draw_keypoint().display();
	feature_mather.add_feature(sift.extract_feature());
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

