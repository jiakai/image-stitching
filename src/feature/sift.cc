/*
 * $File: sift.cc
 * $Date: Tue Apr 30 23:51:42 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "sift_impl.hh"
#include "config.hh"
#include "utils.hh"
#include "draw.hh"
#include "resizer/bilinear.hh"

struct diff_t {
	real_t dx, dy;
};

/*!
 * \brief rotate local image centered at (row, col) and return the gradient
 *		difference
 * note that original pixels are put on grid vertices, with (row, col) at
 * (0, 0), gradient points are sampled on grid centers (.5, .5)
 */
static Matrix<diff_t> rotate_local_image_and_differentiate(const Image &img,
		int row, int col, int radius, real_t angle);

#ifdef DISPLAY_MATDIFF
static void display_matdiff(const Matrix<diff_t> &mat);
#endif

vector<Point> SIFTFeature::Impl::detect() {
	m_stat.reset();
	init_filter();
	search_local_extrema();
	log_printf(
			"detected %d; extrema=%d; rejected: ctr=%d ale=%d no_converge=%d",
			int(m_keypoint.size()),
			m_stat.extrema, m_stat.remove_ctr, m_stat.remove_ale,
			m_stat.adjust_no_converge);
	vector<Point> ret;
	for (auto &p: m_keypoint)
		ret.emplace_back(p.r, p.c);
	return ret;
}

Image SIFTFeature::Impl::draw_keypoint() {
	Image img = m_image.copy();
	for (auto &p: m_keypoint) {
		int r = p.r * img.height(), c = p.c * img.width(),
			radius = min(10, r, c,
				img.height() - r, img.width() - c);
		Point p0(r, c),
			  p1(r + radius * cos(p.orientation),
					  c + radius * sin(p.orientation));
		draw::vector(img, p0, p1);
	}
	return img;
}

void SIFTFeature::Impl::init_filter() {
	int nr_dog = m_detector.m_nr_dog_per_octave;
	assert(nr_dog >= 1);
	m_filters.clear();
	real_t k = pow(m_detector.m_sigma_end / m_detector.m_sigma_start,
			1.0 / nr_dog),
		   sigma = m_detector.m_sigma_start;

	for (int i = 0; i < nr_dog + 3; i ++) {
		m_filters.emplace_back(sigma);
		sigma *= k;
	}
}

void SIFTFeature::Impl::search_local_extrema() {
	Image img_cur_octave;
	BilinearResizer resizer;

	{
		// handle pre-blur and pre-resize
		real_t preblur = m_detector.m_preblur,
			   initsigma = m_detector.m_init_sigma;
		assert(preblur >= initsigma * 2);
		GaussianFilter preblurer(sqrt(preblur * preblur -
					initsigma * initsigma * 4));
		img_cur_octave = preblurer.run(resizer.resize(m_image, 2));
	}

	m_keypoint.clear();

	int min_size = m_detector.m_border_size * 2;

	for (int i = 0; i < m_detector.m_nr_octave; i ++) {
		if (img_cur_octave.width() <= min_size ||
				img_cur_octave.height() <= min_size) {
			break;
		}
		search_local_extrema_in_cur_octave(img_cur_octave);
		img_cur_octave = resizer.resize(img_cur_octave, 0.5);
	}
}

void SIFTFeature::Impl::build_dog_layers(const Image &base_img) {

	// base_img size should be decreasing to avoid extra mem allocation
	// (Image::set_dimension wont reallocate if size decreased)

	m_img_gauss.resize(m_filters.size());
	m_img_dog.resize(m_filters.size() - 1);

	for (size_t i = 0; i < m_filters.size(); i ++) {
		m_filters[i].run(base_img, m_img_gauss[i]);
		if (i)
			m_img_dog[i - 1].assign_as_difference(m_img_gauss[i],
					m_img_gauss[i - 1]);
	}

}

void SIFTFeature::Impl::search_local_extrema_in_cur_octave(const Image &img) {

	build_dog_layers(img);

	/*
	 * uncomment the following lines to save intermediate results 

	{
		static int octave;
		for (size_t i = 0; i < m_img_gauss.size(); i ++)
			m_img_gauss[i].save(DEBUG_DIR"%d-%d-gauss.png",
					octave, int(i));
		for (size_t i = 0; i < m_img_dog.size(); i ++)
			m_img_dog[i].save(DEBUG_DIR"%d-%d-dog.png",
					octave, int(i));
		octave ++;
	}
	*/

	int border_size = m_detector.m_border_size;
	assert(border_size >= 3);

	int width = img.width(), height = img.height();

	pixel_t contrast_threashold = m_detector.m_contrast_threshold;

	for (size_t dog_idx = 1; dog_idx + 1 < m_img_dog.size(); dog_idx ++) {

		Image &diff1 = m_img_dog[dog_idx],
			  &diff0 = m_img_dog[dog_idx - 1],
			  &diff2 = m_img_dog[dog_idx + 1];

		for (int i = border_size; i + border_size < height; i ++) {
			pixel_t *dcur_start = diff1.data(i, 0),
					*dcur = dcur_start + border_size,
					*d0 = diff0.data(i, border_size),
					*d1 = diff2.data(i, border_size),
					*dcur_end = dcur_start + width - border_size;

			for (; dcur < dcur_end; dcur ++, d0 ++, d1 ++) {

				{ //f{{{ check for extrema
					pixel_t register val = dcur[0];

					if (fabs(val) < contrast_threashold) {
						m_stat.remove_ctr ++;
						continue;
					}

#define CMP(op) ( \
		val op dcur[-width - 1] && val op dcur[-width] && val op dcur[-width + 1] && \
		val op dcur[-1] && val op dcur[1] && \
		val op dcur[width - 1] && val op dcur[width] && val op dcur[width + 1] && \
		val op d0[-width - 1] && val op d0[-width] && val op d0[-width + 1] && \
		val op d0[-1] && val op d0[0] && val op d0[1] && \
		val op d0[width - 1] && val op d0[width] && val op d0[width + 1] && \
		val op d1[-width - 1] && val op d1[-width] && val op d1[-width + 1] && \
		val op d1[-1] && val op d1[0] && val op d1[1] && \
		val op d1[width - 1] && val op d1[width] && val op d1[width + 1])

					if (!CMP(<=) && !CMP(>=))
						continue;

#undef CMP
				}
				// f}}}

				m_stat.extrema ++;

				real_t r = i, c = dcur - dcur_start;
				int scale = dog_idx;
				if (!adjust_local_extrema(r, c, scale)) {
					m_stat.remove_ale ++;
					continue;
				}
				real_t sigma = m_filters[scale].sigma();
				add_keypoint(m_img_gauss[scale], r, c,
						iceil(sigma * m_detector.m_orientation_radius),
						sigma * m_detector.m_orientation_sigma);
			}
		}

	}
}

void SIFTFeature::Impl::add_keypoint(const Image &img,
		real_t row, real_t col, int radius, real_t sigma) {

	int nr_bin = m_detector.m_orientation_nr_bin,
		width = img.width(), height = img.height(),
		row0 = ifloor(row), col0 = ifloor(col);
	real_t *hist = m_add_keypoint_hist_mem + 2;
	assert(nr_bin < 360);
	memset(hist, 0, sizeof(real_t) * nr_bin);

	real_t gauss_exp_coeff = -1.0 / (2 * sigma * sigma);

	// since number of keypoints is usually small, and cache can roughly save a
	// sqrt and an atan2, it should be faster to compute here instead of
	// pre-computing all magnitudes and orientations
	// TODO: test the statement above
	for (int r = max(-radius, 1 - row0);
			r <= radius && r + row0 + 1 < height; r ++) {

		const pixel_t *cur_pixel = img.data(r + row0, max(col0 - radius, 1));
		for (int c = max(-radius, 1 - col0);
				c <= radius && c + col0 + 1 < width; c ++, cur_pixel ++) {

			real_t dx = cur_pixel[width] - cur_pixel[-width],
				   dy = cur_pixel[1] - cur_pixel[-1],
				   mag = sqrt(dx * dx + dy * dy),
				   ori = atan2(dy, dx);
			mag *= exp((r * r + c * c) * gauss_exp_coeff);
			int bin = ifloor((ori + M_PI) / (M_PI * 2) * nr_bin);
			if (bin >= nr_bin)
				bin -= nr_bin;
			assert(bin >= 0 && bin < nr_bin);
			hist[bin] += mag;
		}
	}

	hist[-1] = hist[nr_bin - 1];
	hist[nr_bin] = hist[0];
	real_t threshold = hist[0];
	for (int i = 1; i < nr_bin; i ++)
		if (hist[i] > threshold)
			threshold = hist[i];
	threshold *= m_detector.m_orientation_peak_ratio;

	for (int i = 0; i < nr_bin; i ++)
		if (hist[i] >= threshold) {
			real_t bin;
			// interpolate the histogram
			real_t l = hist[i - 1], cur = hist[i], r = hist[i + 1];
			if (fabs(l - cur) < EPS || fabs(r - cur) < EPS)
				bin = i;
			else if (cur > l && cur > r) {
				// parabola fit
				bin = i + (r - l) / (2 * cur - l - r) * 0.5;
			}
			else continue;	// not local peak

			if (bin < 0)
				bin += nr_bin;
			if (bin >= nr_bin)
				bin -= nr_bin;
			real_t orientation = bin / nr_bin * M_PI * 2;
			m_keypoint.emplace_back(
					row / img.height(), col / img.width(),
					orientation,
					calc_descriptor(img, row0, col0, orientation));
		}
}

descriptor_t SIFTFeature::Impl::calc_descriptor(const Image &img,
			int row, int col, real_t base_orientation) {

	int nr_hist = m_detector.m_descr_width, 
		nr_bin = m_detector.m_descr_hist_nr_bin,
		hist_win_width = m_detector.m_descr_hist_win_width;
	assert(!(nr_hist % 2));
	int radius = nr_hist * hist_win_width / 2;
	assert(radius * sqrt(2) < m_detector.m_border_size);

	real_t gauss_exp_coeff = -1.0 / (2 * radius * radius);

	Matrix<diff_t> img_diff = rotate_local_image_and_differentiate(
			img, row, col, radius, -base_orientation);
#ifdef DISPLAY_MATDIFF
	display_matdiff(img_diff);
#endif

	m_descr_size = nr_bin * sqr(nr_hist);
	descriptor_t retval(create_auto_buf<real_t>(m_descr_size, true));
	real_t *dest_hist = retval.get();

	for (int i = 0; i < nr_hist; i ++)
		for (int j = 0; j < nr_hist; j ++) {
			for (int r = 0; r < hist_win_width; r ++) {
				int row0 = i * hist_win_width + r,
					col0 = j * hist_win_width;
				real_t x = row0 + 0.5 - radius;
				diff_t *diff_row = img_diff.data(row0, col0);

				for (int c = 0; c < hist_win_width; c ++) {
					real_t y = col0 + c + 0.5 - radius,
						   dx = diff_row[c].dx,
						   dy = diff_row[c].dy,
						   mag = sqrt(dx * dx + dy * dy) *
							   exp(gauss_exp_coeff * (x * x + y * y)),
						   ori = atan2(dy, dx);
					if (ori < 0)
						ori += M_PI * 2;
					int bin = int(ori / (M_PI * 2) * nr_bin);
					if (bin >= nr_bin)
						bin -= nr_bin;
					assert(bin >= 0 && bin < nr_bin);
					dest_hist[bin] += mag;
				}
			}
			dest_hist += nr_bin;
		}

	dest_hist = retval.get();
	real_t norm = 1.0 / calc_length(dest_hist, m_descr_size),
		   thr = m_detector.m_descr_len_thr;
	for (int i = 0; i < m_descr_size; i ++) {
		dest_hist[i] *= norm;
		if (dest_hist[i] >= thr)
			dest_hist[i] = thr;
	}
	norm = 1.0 / calc_length(dest_hist, m_descr_size);
	for (int i = 0; i < m_descr_size; i ++)
		dest_hist[i] *= norm;

	return retval;
}

vector<FeatureKeypoint> SIFTFeature::Impl::extract_feature() {
	vector<FeatureKeypoint> ret(m_keypoint.size());
	for (size_t i = 0; i < m_keypoint.size(); i ++) {
		ret[i].r = m_keypoint[i].r;
		ret[i].c = m_keypoint[i].c;
		ret[i].descriptor.resize(m_descr_size);
		memcpy(ret[i].descriptor.data(), m_keypoint[i].descriptor.get(),
				sizeof(real_t) * m_descr_size);
	}
	return ret;
}

SIFTFeature::SIFTFeature(const Image &img):
	FeatureBase(img),
	m_impl(new Impl(*this, m_image)),
	m_nr_octave(config.get_int("SIFT.NR_OCTAVE", 10)),
	m_nr_dog_per_octave(config.get_int("SIFT.NR_DOG_PER_OCTAVE", 3)),
	m_border_size(config.get_int("SIFT.BORDER_SIZE", 12)),
	m_orientation_nr_bin(config.get_int("SIFT.ORIENTATION_NR_BIN", 36)),
	m_subpxlrefine_nr_iter(config.get_int("SIFT.SUBPXLREFINE_NR_ITER", 10)),
	m_descr_hist_win_width(config.get_int("SIFT.DESCR_HIST_WIN_WIDTH", 4)),
	m_descr_hist_nr_bin(config.get_int("SIFT.DESCR_HIST_NR_BIN", 8)),
	m_descr_width(config.get_int("SIFT.DESCR_WIDTH", 4)),
	m_max_image_size(config.get_int("SIFT.MAX_IMAGE_SIZE", 800)),
	m_contrast_threshold(config.get_double("SIFT.CONTRAST_THRESHOLD", 0.015)),
	m_preblur(config.get_double("SIFT.PREBLUR", 1.6)),
	m_init_sigma(config.get_double("SIFT.INIT_SIGMA", 0.5)),
	m_sigma_start(config.get_double("SIFT.SIGMA_START", 1.6)),
	m_sigma_end(config.get_double("SIFT.SIGMA_END", 3.2)),
	m_edge_threshold(config.get_double("SIFT.EDGE_THRESHOLD", 10)),
	m_orientation_sigma(config.get_double("SIFT.ORIENTATION_SIGMA", 1.5)),
	m_orientation_radius(config.get_double("SIFT.ORIENTATION_RADIUS", 4.5)),
	m_orientation_peak_ratio(config.get_double("SIFT.ORIENTATION_PEAK_RATIO", 0.8)),
	m_descr_len_thr(config.get_double("SIFT.DESCR_LEN_THR", 0.2))

{
	int max_side = max(img.width(), img.height());
	if (max_side > m_max_image_size) {
		real_t f = real_t(m_max_image_size) / max_side;
		BilinearResizer resizer;
		m_image = resizer.resize(img, f);
		log_printf("SIFT: resize input image: %.4f", f);
	}
}

SIFTFeature::~SIFTFeature() {
	delete m_impl;
}

vector<Point> SIFTFeature::detect() {
	return m_impl->detect();
}


Image SIFTFeature::draw_keypoint() {
	return m_impl->draw_keypoint();
}

vector<FeatureKeypoint> SIFTFeature::extract_feature() {
	return m_impl->extract_feature();
}


Matrix<diff_t> rotate_local_image_and_differentiate(const Image &img,
		int row, int col, int radius, real_t angle) {

	thread_local static Matrix<diff_t> result;
	result.set_dimension(radius * 2, radius * 2);

	real_t cos_agl = cos(angle), sin_agl = sin(angle);
	int width = img.width();

	diff_t *dest = result.data();
	for (int dest_r = -radius + 1; dest_r <= radius; dest_r ++) {
		/*
		 * dest_r = actual destination row + 0.5
		 * note that r corresponds to x and c corresponds to y, in the
		 * right-hand coordinate system
		 * r_orig = cos * r + sin * c
		 * c_orig = -sin * r + cos * c
		 */
		real_t r0 = dest_r - 0.5;
		for (int dest_c = -radius + 1; dest_c <= radius; dest_c ++, dest ++) {
			real_t c0 = dest_c - 0.5,
				   orig_r = cos_agl * r0 + sin_agl * c0 + row,
				   orig_c = -sin_agl * r0 + cos_agl * c0 + col;
			int orig_ri = ifloor(orig_r),
				orig_ci = ifloor(orig_c);
			real_t x = orig_r - orig_ri, y = orig_c - orig_ci;
			const pixel_t *src = img.data(orig_ri, orig_ci),
				  p0 = src[0], p2 = src[1],
				  p1 = src[width], p3 = src[width + 1];
			/*
			 * linear interpolation:
			 * f(x, y) = p0*(1 - x)*(1 - y) + p1*x*(1 - y) +
			 *			 p2*(1 - x)*y + p3*x*y
			 * goal is to compute grad(f) \dot V,
			 * where V is the direction of (1, 0) and (0, 1) after
			 * rotation
			 */

			real_t fdx = (p1 - p0) * (1 - y) + (p3 - p2) * y,
				   fdy = (p2 - p0) * (1 - x) + (p3 - p1) * x;
			dest->dx = fdx * cos_agl - fdy * sin_agl;
			dest->dy = fdx * sin_agl + fdy * cos_agl;

		}
	}

	return result;
}

#ifdef DISPLAY_MATDIFF
void display_matdiff(const Matrix<diff_t> &mat)  {
	constexpr int VECSIZE = 32;
	int width = mat.width(), height = mat.height();
	Image img = Image::create(VECSIZE * width, VECSIZE * height);
	real_t max = 0;
	for (int i = 0; i < width * height; i ++)
		max = std::max(max, quadratic_sum_sqrt(mat.data()[i].dx, mat.data()[i].dy));

	for (int r = 0; r < height; r ++)
		for (int c = 0; c < width; c ++) {
			int tr = r * VECSIZE + VECSIZE / 2,
				tc = c * VECSIZE + VECSIZE / 2;
			const diff_t &d = mat.at(r, c);
			real_t fac = 1.0 / max * VECSIZE * 0.5;
			draw::vector(img, Point(tr, tc),
					Point(tr + d.dx * fac, tc + d.dy * fac));
		}

	static int num;
	img.save(DEBUG_DIR "matdiff-%d.png", num ++);
}
#endif

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

