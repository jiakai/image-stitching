/*
 * $File: naivematcher.cc
 * $Date: Mon Apr 29 10:01:40 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "feature/naivematcher.hh"
#include "config.hh"
#include "math.hh"

#include <limits>

NaiveFeatureMatcher::NaiveFeatureMatcher(
		const FeatureKeypointArray &feature_set):
	FeatureMatcherBase(feature_set),
	m_ratio_threshold(config.get_double("FEATURE_DIST_RATIO_THRESHOLD", 0.8))
{
}

const FeatureKeypoint* NaiveFeatureMatcher::match(
		const FeatureKeypoint &key, real_t *out_dist) const {
	const FeatureKeypoint *min_pos = nullptr;
	real_t min_val0 = std::numeric_limits<real_t>::max(), min_val1 = min_val0;
	for (auto &p: m_feature_set) {
		real_t dist = calc_euclidean_dist(key.descriptor.data(),
				p.descriptor.data(), key.descriptor.size());

		if (dist < min_val0) {
			min_val1 = min_val0;
			min_val0 = dist;
			min_pos = &p;
		} else if (dist < min_val1) {
			min_val1 = dist;
		}
	}

	if (min_val0 <= min_val1 * m_ratio_threshold) {
		if (out_dist)
			*out_dist = min_val0;
		return min_pos;
	}

	return nullptr;
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

