/*
 * $File: imgio.cc
 * $Date: Tue Apr 09 19:46:36 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "imgio.hh"
#include "utils.hh"

#include <cstring>

#include <Magick++.h>
using namespace Magick;

using MImage = ::Image;

static Magick::Image _convert_rgb_to_img(const MImage &ch_r,
		const MImage &ch_g, const MImage &ch_b);

static void _init_image_magick() {
	InitializeMagick(nullptr);
}
REGISTER_INIT_FUNC(_init_image_magick);

ImageReader::ImageReader(const char *fpath) {
	if (fpath)
		read(fpath);
}

void ImageReader::read(const char *fpath) {
	Magick::Image image;
	image.read(fpath);

	m_width = image.size().width();
	m_height = image.size().height();

	Blob blob;
	image.write(&blob, "RGB", 8);
	for (auto &i: m_color_channel)
		i.reset(new pixel_t[m_width * m_height],
				[](pixel_t* p){delete []p;});

	for (int k = 0; k < 3; k ++) {
		pixel_t *t = m_color_channel[k].get();
		auto s = static_cast<const uint8_t*>(blob.data()) + k;
		for (int cnt = m_height * m_width; cnt --; ) {
			*(t ++) = *s / 255.0;
			s += 3;
		}
	}

	m_gray.reset(new pixel_t[m_width * m_height],
			[](pixel_t *p){delete []p;});

	{
		pixel_t *t = m_gray.get();
		auto s = static_cast<const uint8_t*>(blob.data());
		for (int cnt = m_height * m_width; cnt --; ) {
			*(t ++) = (0.21 * s[0] + 0.71 * s[1] + 0.08 * s[2]) / 255.0;
			s += 3;
		}
	}
}

void save_image(const char *fpath,
		const MImage &ch_r, const MImage &ch_g, const MImage &ch_b) {

	_convert_rgb_to_img(ch_r, ch_g, ch_b).write(fpath);
}


void display_image(const MImage &ch_r, const MImage &ch_g, const MImage &ch_b) {
	_convert_rgb_to_img(ch_r, ch_g, ch_b).display();
}

Magick::Image _convert_rgb_to_img(const MImage &ch_r, const MImage &ch_g,
		const MImage &ch_b) {

	int width = -1, height = -1;

	for (auto i: {&ch_r, &ch_g, &ch_b}) {
		if (!i->is_black()) {
			if (width == -1) {
				width = i->width();
				height = i->height();
			}
			assert(width == i->width());
			assert(height == i->height());
		}
	}
	assert(width != -1);

	size_t data_len = width * height * 3;
	uint8_t *data = new uint8_t[data_len];

	memset(data, 0, data_len);
	{
		uint8_t* data_start = data;
		for (auto i: {&ch_r, &ch_g, &ch_b}) {
			if (!i->is_black()) {
				const pixel_t *s = i->data();
				uint8_t *t = data_start;
				for (int cnt = width * height; cnt --; ) {
					pixel_t register x = *(s ++);
					if (x < 0) x = 0;
					if (x > 1) x = 1;
					*t = static_cast<uint8_t>(x * 255);
					t += 3;
				}
			}
			data_start ++;
		}
	}

	Magick::Image img(Blob(data, data_len), Geometry(width, height),
			8, "RGB");
	delete []data;
	return img;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

