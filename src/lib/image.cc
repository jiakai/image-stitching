/*
 * $File: image.cc
 * $Date: Sat Apr 06 23:46:46 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "image.hh"
#include "imgio.hh"

#include <cstring>
#include <cstdio>
#include <cstdarg>
#include <cmath>

#ifndef PATH_MAX
#define PATH_MAX	255
#endif

Image Image::m_black;

Image Image::create(int width, int height)
{
	return Image(width, height, create_auto_buf<pixel_t>(width * height, true));
}

void Image::display() const {
	display_image(*this);
}

void Image::save(const char *fpath_fmt, ...) const {
	char fpath[PATH_MAX];
	va_list ap;
	va_start(ap, fpath_fmt);
	vsnprintf(fpath, PATH_MAX, fpath_fmt, ap);
	va_end(ap);

	save_image(fpath, *this);
}

Image Image::copy() const {
	Image ret = create(m_width, m_height);
	memcpy(ret.data(), data(), sizeof(pixel_t) * m_width * m_height);
	return ret;
}

Image Image::operator - (const Image &img) const {
	Image ret;
	ret.assign_as_difference(*this, img);
	return ret;
}

Image& Image::assign_as_difference(const Image &a, const Image &b) {
	assert(a.m_width == b.m_width);
	assert(a.m_height == b.m_height);
	set_dimension(a);
	pixel_t *dest = m_data.get();
	const pixel_t *src0 = a.data(), *src1 = b.data();
	for (int i = m_width * m_height; i --; )
		*(dest ++) = *(src0 ++) - *(src1 ++);
	return *this;
}

Image& Image::linear_normalize() {
	pixel_t *pix = data();
	pixel_t min = pix[0], max = pix[0];
	for (int i = m_width * m_height; i --; ) {
		if (*pix < min)
			min = *pix;
		if (*pix > max)
			max = *pix;
		pix ++;
	}

	pix = data();
	pixel_t k = 1.0 / (max - min);
	for (int i = m_width * m_height; i --; ) {
		*pix = (*pix - min) * k;
		pix ++;
	}

	return *this;
}

Image& Image::gamma_correction(real_t gamma) {
	pixel_t register *p = data(), *pt = p + m_width * m_height;
	while (p != pt) {
		*p = pow(*p, gamma);
		p ++;
	}
	return *this;
}


// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

