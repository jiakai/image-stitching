/*
 * $File: config.cc
 * $Date: Mon Apr 29 11:55:52 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "config.hh"
#include "utils.hh"
#include "exc.hh"

#include <cstdio>

#include <map>
#include <string>
#include <fstream>
#include <sstream>
#include <functional>
#include <vector>
using namespace std;

using real_t_vector = vector<real_t>;

class Config::KVMap {
	public:
		map<string, string> m_map;

		KVMap(const char *fpath) {
			ifstream fin(fpath);
			if (!fin.good())
				return;
			string line;
			while (getline(fin, line)) {
				istringstream sin(line);
				string k, v;
				sin >> k >> v;
				if (sin)
					m_map[k] = v;
			}
		}


		template<typename T>
		T get(const string &key, function<T(const string &val)> converter,
				const T&def) {
			auto pos = m_map.find(key);
			if (pos == m_map.end())
				return def;
			log_printf("warning: option %s overwritten in config file: %s",
					key.c_str(), pos->second.c_str());
			return converter(pos->second);
		}
};

Config::Config(const char *fpath):
	m_map(new KVMap(fpath))
{
}

Config::~Config() {
	delete m_map;
}

int Config::get_int(const char *key, int default_val) {
	return m_map->get<int>(key,
			[](const string &val) -> int {
				int v;
				if (sscanf(val.c_str(), "%d", &v) != 1)
					throw IstitcherError("can not convert to int: %s",
						val.c_str());
				return v;
			},
			default_val);
}

double Config::get_double(const char *key, double default_val) {
	return m_map->get<double>(key,
			[](const string &val) -> double {
				double v;
				if (sscanf(val.c_str(), "%lf", &v) != 1)
					throw IstitcherError("can not convert to int: %s",
						val.c_str());
				return v;
			},
			default_val);
}

real_t_vector Config::get_real_t_vector(const char *key,
		const real_t_vector &default_val) {

	return m_map->get<real_t_vector>(key,
			[](const string &val) -> real_t_vector {
				real_t_vector rst;
				istringstream sin(val);
				for (real_t x; sin >> x; rst.push_back(x));
				return rst;
			},
			default_val);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

