/*
 * $File: stitcher_impl.hh
 * $Date: Wed May 08 20:37:42 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "stitcher.hh"
#include "stitcher_mat.hh"
#include "stitcher_projector.hh"
#include "vector.hh"

#include <functional>
#include <vector>
using namespace std;

namespace stitcher_impl {
	using ImageArray = ImageStitcher::ImageArray;

	/*!
	 * \brief represent a component on the composed image (i.e corresponds to an
	 *		input image)
	 */
	struct ImageComponent {
		Homography homography, homography_inv;
		size_t image_idx;	//! index in the input image array

		ImageComponent(size_t idx):
			image_idx(idx)
		{ }
	};

	enum class ProjectionMethod {
		FLAT, CYLINDRICAL
	};

	class GainCompensator {
		// f{{{ private impl
		struct MatchedImage {
			int v0, v1;
			Homography trans;
			MatchedImage(int v0_, int v1_, const Homography &trans_):
				v0(v0_), v1(v1_), trans(trans_)
			{}
		};
		struct MatchedBlock {
			real_t u0_sum, u1_sum;
			int cnt;
		};

		vector<MatchedImage> m_matched_img;
		int m_fit_order;

		/*!
		 * \brief build and solve equations from input image
		 * \return coefficients for each transformation
		 */
		vector<real_t> solve_coeff(const ImageArray &image) const;

		/*!
		 * \brief return the index in m_coeff of a specified coefficient in an
		 *			equation
		 */
		int get_coeff_idx(int eqn_idx, int order) const {
			return eqn_idx * m_fit_order + order;
		}

		real_t get_entropy(const Image &img) const;

		// f}}}

		public:
			bool m_enable;
			vector<real_t> m_fit_term_scale;	/*! determines how much the
												  variance of coefficient of
												  each term contributes to total
												  error */
			int m_sample_size,	//! size of each sampling block
				m_entropy_nr_quantum;

			GainCompensator();

			/*!
			 * \brief add a pair of matched points
			 * \param v0 index of the image; starting from 0, continously
			 *		numbered; note that v0 must be different from v1;
			 *		for a pair (v0, v1), both (v0, v1) and (v1, v0) should be
			 *		added
			 * \param trans transform from v0 to v1
			 */
			void add(int v0, int v1, const Homography &trans) {
				assert(v0 != v1);
				m_matched_img.emplace_back(v0, v1, trans);
			}

			ImageArray run(const ImageArray &input_image,
					const ImageArray &feature_image) const;
	};

	/*!
	 * \brief represent a composed image
	 */
	struct ComposedImage {
		ProjectionMethod projection_method;
		size_t identity_image_idx;	/*! the image with identity homograph; used
									  to determine aspect of output */
		Vector2D proj_min, proj_max;
		vector<ImageComponent> component;

		function<void()> update_proj_range;
		function<void(real_t &xmin, real_t &ymin, real_t &xmax, real_t &ymax)>
			update_img_proj_range;

		GainCompensator gain_compensator;
	};

	class BundleAdjustor {

		// f{{{ private
		struct Term {
			bool is_outlier;
			real_t coeff;
			int idx0, idx1;
			Vector2D pos0, pos1;
			Term(real_t coeff_, int idx0_, int idx1_,
					const Vector2D &pos0_, const Vector2D &pos1_):
				coeff(coeff_),
				idx0(idx0_), idx1(idx1_),
				pos0(pos0_), pos1(pos1_)
			{}
		};
		std::vector<bool> m_preserve;
		std::vector<int> m_homo_idxtrans;
		std::vector<Term> m_term;
		std::vector<Homography*> m_homo_orig;
		std::vector<Homography> m_homo, m_homo_new;
		int m_nr_unknown_homo;

		void single_step(real_t lambda);
		real_t calc_error(const std::vector<Homography> &homo,
				int *nr_outlier);

		inline int get_coeff_idx(int homo_idx, int r, int c);
		void init_homo_and_idxtrans();
		void extend_size(int idx);

		// f}}}

		public:
			bool m_enable;
			real_t m_lambda_init, m_lambda_chg, m_lambda_max,
				   m_outlier_thresh = numeric_limits<real_t>::max();
			int m_nr_iter;

			struct Stat {
				real_t err_before, err_after;
				int nr_outlier;
			};

			BundleAdjustor();

			/*!
			 * \brief add a matched pair; each pair should be added only once
			 */
			void add_term(real_t coeff, int idx0, const Vector2D &pos0,
					int idx1, const Vector2D &pos1);

			/*!
			 * \brief add a homography, which, after run, would be modified in
			 *		place
			 */
			void add_homography(int idx, Homography &h);

			/*!
			 * \param preserve which homography should be preserved after
			 *		adjusting
			 * \return new_err/orig_err
			 */
			Stat run();

			/*!
			 * \brief preserve a homography
			 */
			void set_preserve(int idx, bool flag = true);

	};

}
using namespace stitcher_impl;

class ImageStitcher::Impl {
	using homo2proj_t = projector::homo2proj_t;
	using proj2homo_t = projector::proj2homo_t;

	struct PairwiseMatchInfo {
		MatchedPointArray matched;
		real_t error = numeric_limits<real_t>::max();
		Homography trans;
	};

	ImageStitcher &m_stitcher;
	ImageArray m_feature_image;

	vector<ComposedImage> m_composed;
	vector<vector<bool>> m_mst_connected; /*! m_mst_connected[i][j]: whether
											image i, j connected in mst */
	vector<vector<PairwiseMatchInfo>> m_pairwise_match;

	void calc_pairwise_match(const FeatureMatcherPtrArray &matcher);
	void calc_mst();
	void calc_projection_param(ComposedImage &composed) const;
	void calc_projection_range(ComposedImage &composed) const;
	void choose_identity_image(ComposedImage &composed) const;
	void rotate_for_cylindrical_proj(ComposedImage &composed) const;
	void horiz_straighten(ComposedImage &composed) const;
	/*!
	 * \brief compute a homography matrix that transforms (x0, y0) to (x1, y1)
	 */
	void calc_homography(
			const MatchedPointArray &match, PairwiseMatchInfo &match_info) const;
	void bundle_adjust(ComposedImage &composed) const;

	Image do_compose(
			const ComposedImage &composed, const ImageArray &input_image)const;

	template <homo2proj_t homo2proj, proj2homo_t proj2homo>
	Image do_compose_with_homotrans(
			const ComposedImage &composed, const ImageArray &input_image)const;

	/*!
	 * \brief blend all images
	 * note that projection_x = target_row * scale_x + offset_x
	 * \param input_image input_image[i] should be the corresponding image of
	 *		component[i]
	 */
	template <homo2proj_t homo2proj, proj2homo_t proj2homo>
	void blend(Image &target, 
			real_t scale_x, real_t scale_y,
			real_t offset_x, real_t offset_y,
			const ImageArray &input_image,
			const ComposedImage &composed) const;

	void draw_match(
			const MatchedPointArray &match, const Homography &trans,
			size_t idx0, size_t idx1) const;

	void dump_homo_point(const ComposedImage &composed) const;

	public:
		Impl(ImageStitcher &st);

		void init(const FeatureMatcherPtrArray &matcher,
				const ImageArray &feature_image);

		ImageArray stitch(const ImageArray &image) const;
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

