/*
 * $File: stitcher_debug.hh
 * $Date: Tue Apr 30 17:54:58 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "debug_output.hh"
#include "stitcher_impl.hh"

static inline std::ostream& operator <<
		(std::ostream &ostr, const Homography &h) {
	real_t *mat = (real_t*)(&h);
	for (int i = 0; i < 3; i ++) {
		bool first = true;
		for (int j = 0; j < 3; j ++) {
			if (first)
				first = false;
			else
				ostr << ' ';
			ostr << mat[i * 3 + j];
		}
		ostr << endl;
	}
	return ostr;
}


// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

