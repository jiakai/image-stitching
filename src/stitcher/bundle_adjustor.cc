/*
 * $File: bundle_adjustor.cc
 * $Date: Wed May 08 20:36:23 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "stitcher_impl.hh"
#include "stitcher_mat.hh"
#include "config.hh"
#include "utils.hh"
#include "stitcher_debug.hh"

namespace stitcher_impl {
	namespace bundle_adjustor_impl {
		class FlatErrFunc {
			// f{{{
			Vector3D m_x0;
			real_t m_c;
			int m_idx;
			public:
				/*!
				 * \brief reprsent the function x[idx]/x[2] + c
				 */
				FlatErrFunc(int idx, real_t c, const Vector3D &x0):
					m_x0(x0), m_c(c), m_idx(idx)
				{}

				real_t eval() const {
					return m_x0[m_idx] / m_x0.z + m_c;
				}

				real_t diff(int i) const {
					if (i < 2)
						return i == m_idx ? 1 / m_x0.z : 0;
					return -m_x0[m_idx] / sqr(m_x0.z);
				}

				static constexpr int NR_FUNC = 2;

				static Vector3D get_normalized(const Vector3D &v) {
					Vector3D rst(v.x / v.z, v.y / v.z, 1);
					assert(std::isfinite(rst.x) && std::isfinite(rst.y));
					return rst;
				}
		};
		// f}}}

		class SphericalErrFunc {
		// f{{{
			Vector3D m_x0;
			real_t m_c;
			int m_idx;
			public:
				/*!
				 * \brief reprsent the function x[idx]/x.mod() + c
				 */
				SphericalErrFunc(int idx, real_t c, const Vector3D &x0):
					m_x0(x0), m_c(c), m_idx(idx)
				{}

				real_t eval() const {
					return m_x0[m_idx] / m_x0.mod() + m_c;
				}

				real_t diff(int i) const {
					real_t ms = m_x0.mod_sqr();
					if (i == m_idx)
						return (ms - sqr(m_x0[i])) * pow(ms, -1.5);
					return -m_x0[i] * m_x0[m_idx] * pow(ms, -1.5);
				}

				real_t diff(int i, int j) const {
					return 0; // XXX
				}

				static constexpr int NR_FUNC = 3;

				static Vector3D get_normalized(const Vector3D &v) {
					return v.get_normalized();
				}
		};
		// f}}}
	}
}
using namespace stitcher_impl::bundle_adjustor_impl;
using ErrFunc = SphericalErrFunc;

BundleAdjustor::Stat BundleAdjustor::run() {
	init_homo_and_idxtrans();
	Stat stat{0, 0, 0};

	real_t lambda = m_lambda_init, prev_err = calc_error(m_homo, &stat.nr_outlier);
	stat.err_before = prev_err;
	if (fabs(prev_err) < EPS || !m_enable) {
		stat.err_after = prev_err;
		return stat;
	}

	for (int i = 0; i < m_nr_iter && lambda < m_lambda_max; i ++) {
		single_step(lambda);
		real_t err = calc_error(m_homo_new, nullptr);
		if (err < prev_err) {
			prev_err = err;
			m_homo = m_homo_new;
			lambda /= m_lambda_chg;
		} else {
			lambda *= m_lambda_chg;
		}
	}

	for (size_t i = 0; i < m_homo.size(); i ++)
		if (m_homo_orig[i])
			*m_homo_orig[i] = m_homo[i];

	stat.err_after = prev_err;
	return stat;
}

void BundleAdjustor::init_homo_and_idxtrans() {
	m_homo.resize(m_homo_orig.size());
	m_homo_idxtrans.resize(m_homo_orig.size());
	int idx = 0;
	for (size_t i = 0; i < m_homo.size(); i ++) {
		int t;
		if (m_homo_orig[i]) {
			m_homo[i] = *m_homo_orig[i];
			if (m_preserve[i])
				t = -1;
			else
				t = idx ++;
		} else
			t = -1;
		m_homo_idxtrans[i] = t;
	}
	m_nr_unknown_homo = idx;
}

void BundleAdjustor::set_preserve(int idx, bool flag) {
	assert(idx >= 0 && idx < int(m_preserve.size()));
	m_preserve[idx] = flag;
}

void BundleAdjustor::add_term(real_t coeff, int idx0, const Vector2D &pos0,
		int idx1, const Vector2D &pos1) {

	extend_size(idx0);
	extend_size(idx1);
	m_term.emplace_back(coeff, idx0, idx1, pos0, pos1);
}

void BundleAdjustor::add_homography(int idx, Homography &h) {
	extend_size(idx);
	m_homo_orig[idx] = &h;
}

void BundleAdjustor::extend_size(int idx) {
	if (idx >= int(m_homo_orig.size())) {
		m_homo_orig.resize(idx + 1);
		m_preserve.resize(idx + 1);
	}
}

real_t BundleAdjustor::calc_error(
		const std::vector<Homography> &homo, int *nr_outlier){

	real_t sum = 0;
	for (auto &t: m_term) {
		if (!m_homo_orig[t.idx0] || !m_homo_orig[t.idx1])
			continue;
		Vector3D p0(ErrFunc::get_normalized(homo[t.idx0].trans(t.pos0))),
				 p1(ErrFunc::get_normalized(homo[t.idx1].trans(t.pos1)));
		real_t err = (p0 - p1).mod_sqr();
		sum += err * t.coeff;
		if (nr_outlier) {
			if (err > m_outlier_thresh) {
				t.is_outlier = true;
				(*nr_outlier) ++;
			} else
				t.is_outlier = false;
		}
	}
	return sum;
}

void BundleAdjustor::single_step(real_t lambda) {
	int nr_unknown = m_nr_unknown_homo * 8;
	Matrix<real_t> hessian;
	vector<real_t> derivative(nr_unknown);
	hessian.reset(nr_unknown, nr_unknown);

	real_t term_coeff = 1;
	auto add_derivative = [&derivative, &hessian, this, &term_coeff]
			(const Vector3D &pother_norm, int homo_num, const Vector3D &p0,
				 const Vector3D &p0_trans) {
			
			if (m_preserve[homo_num])
				return;

			int nr_derv = 0, derv_pos[8];
			real_t derv_val[8];
			for (int i = 0; i < 3; i ++) {
				real_t diff = 0;
				for (int j = 0; j < ErrFunc::NR_FUNC; j ++) {
					ErrFunc f(j, -pother_norm[j], p0_trans);
					diff += f.eval() * f.diff(i);
				}
				for (int j = 0; j < (i == 2 ? 2 : 3); j ++) {
					int p = get_coeff_idx(homo_num, i, j);
					real_t d = diff * p0[j];
					derivative[p] += d * term_coeff;
					derv_pos[nr_derv] = p;
					derv_val[nr_derv] = d;
					nr_derv ++;
				}
			}
			assert(nr_derv == 8);
			for (int i = 0; i < 8; i ++) {
				real_t a = derv_val[i];
				int p0 = derv_pos[i];
				for (int j = 0; j < 8; j ++) {
					real_t b = derv_val[j];
					int p1 = derv_pos[j];
					if (i == j)
						hessian.at(p0, p1) += b * b * term_coeff;
					else {
						hessian.at(p0, p1) += a * b * term_coeff;
						hessian.at(p1, p0) += b * a * term_coeff;
					}
				}
			}
		};

	for (auto &term: m_term) {
		if (term.is_outlier)
			continue;

		int idx0 = term.idx0, idx1 = term.idx1;
		if (!m_homo_orig[idx0] || !m_homo_orig[idx1])
			continue;

		term_coeff = term.coeff;

		Vector3D p0(term.pos0.x, term.pos0.y, 1),
				 p1(term.pos1.x, term.pos1.y, 1);
		const Homography &h0 = m_homo[idx0], &h1 = m_homo[idx1];
		Vector3D p0_trans(h0.trans(p0)), p1_trans(h1.trans(p1)),
				 p0_tnorm(ErrFunc::get_normalized(p0_trans)),
				 p1_tnorm(ErrFunc::get_normalized(p1_trans));

		add_derivative(p1_tnorm, idx0, p0, p0_trans);
		add_derivative(p0_tnorm, idx1, p1, p1_trans);

		if (m_preserve[idx0] || m_preserve[idx1])
			continue;

		for (int fidx = 0; fidx < ErrFunc::NR_FUNC; fidx ++) {
			ErrFunc f0(fidx, -p1_tnorm[fidx], p0_trans),
					f1(fidx, -p0_tnorm[fidx], p1_trans);
			for (int i = 0; i < 3; i ++)
				for (int j = 0; j < 3; j ++) {
					real_t diff_01 = -f0.diff(i) * f1.diff(j);

					for (int ki = 0; ki < (i == 2 ? 2 : 3); ki ++) {
						int hidx0 = get_coeff_idx(idx0, i, ki);

						for (int kj = 0; kj < (j == 2 ? 2 : 3); kj ++) {
							int hidx1 = get_coeff_idx(idx1, j, kj);
							real_t diff = diff_01 * p0[ki] * p1[kj] *
								term_coeff;
							hessian.at(hidx0, hidx1) += diff;
							hessian.at(hidx1, hidx0) += diff;
						}
					}
				}
		}
	}

	for (int i = 0; i < nr_unknown; i ++)
		hessian.at(i, i) *= 1 + lambda;

	auto delta = solve_linear_eqn(hessian, derivative);

	m_homo_new.resize(m_homo.size());
	for (size_t i = 0; i < m_homo_new.size(); i ++) {
		if (!m_homo_orig[i])
			continue;

		if (m_preserve[i]) {
			m_homo_new[i] = m_homo[i];
			continue;
		}
		real_t *mt = m_homo_new[i].mat;
		const real_t *ms = m_homo[i].mat;
		mt[8] = ms[8];
		int offset = get_coeff_idx(i, 0, 0);
		for (int j = 0; j < 8; j ++)
			mt[j] = ms[j] - delta[offset + j];
	}
}

int BundleAdjustor::get_coeff_idx(int homo_idx, int r, int c) {
	assert(homo_idx >= 0 && homo_idx < int(m_homo.size()));
	homo_idx = m_homo_idxtrans[homo_idx];
	assert(homo_idx != -1);
	assert(r >= 0 && c >= 0 && ((r < 2 && c < 3) || (r == 2 && c < 2)));
	return homo_idx * 8 + r * 3 + c;
}

BundleAdjustor::BundleAdjustor():
	m_enable(config.get_int("BUNDLE_ADJUSTOR.ENABLE", 1)),
	m_lambda_init(config.get_double("BUNDLE_ADJUSTOR.INIT_LAMBDA", 0.1)),
	m_lambda_chg(config.get_double("BUNDLE_ADJUSTOR.LAMBDA_CHG", 2)),
	m_lambda_max(config.get_double("BUNDLE_ADJUSTOR.LAMBDA_MAX", 1e10)),
	m_nr_iter(config.get_int("BUNDLE_ADJUSTOR.NR_ITER", 500))
{
}

static void test_ba() {
	BundleAdjustor adj;
	real_t mat0[9] = {
			1, 0, 0,
			0, 1, 0,
			0, 0, 1,
		}, mat1[9] = {
			1, 0, 0,
			0, 1, 0,
			0, 0, 1
		};
	Homography h0(mat0), h1(mat1), h2(mat1);
	mat1[0] = 2;
	Homography h1_real(mat1),
			   h1_real_inv = h1_real.get_inv();
	mat1[4] = 2;
	Homography h2_real(mat1),
			   h2_real_inv = h2_real.get_inv();
	for (int i = 0; i < 4; i ++) {
		Vector2D p0(i / 2, i % 2);
		Vector3D pt(h0.trans(p0));
		Vector2D p1(h1_real_inv.trans_normalized(pt)),
				 p2(h2_real_inv.trans_normalized(pt));
		adj.add_term(1, 3, p0, 4, p1);
		adj.add_term(1, 4, p1, 5, p2);
	}
	adj.add_term(1, 0, Vector2D(0, 0), 1, Vector2D(5, 5));	// test unused term
	adj.add_homography(3, h0);
	adj.add_homography(4, h1);
	adj.add_homography(5, h2);
	adj.set_preserve(3);

	adj.run();
	cout << "h1:\n" << h1;
	cout << "h2:\n" << h2;
	for (int i = 0; i < 4; i ++) {
		Vector2D p0(i / 2, i % 2);
		Vector2D p1(h1_real_inv.trans_normalized(h0.trans(p0)));

		cout << p0 << ", " << p1 << ": " << h0.trans_normalized(p0) << ' '
			<< h1.trans_normalized(p1) << endl;
	}
	assert(0);
}
// REGISTER_INIT_FUNC(test_ba);

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

