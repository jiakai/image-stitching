/*
 * $File: stitcher_projector.hh
 * $Date: Tue Apr 23 10:41:31 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "vector.hh"

namespace stitcher_impl {
	namespace projector {
		typedef Vector2D (*homo2proj_t)(const Vector3D&);
		typedef Vector3D (*proj2homo_t)(const Vector2D&);

		namespace flat {

			static inline Vector2D homo2proj(const Vector3D &coord) {
				return Vector2D(coord.x / coord.z, coord.y / coord.z);
			}

			static inline Vector3D proj2homo(const Vector2D &coord) {
				return Vector3D(coord.x, coord.y, 1);
			}
		}


		namespace cylindrical {

			static inline Vector2D homo2proj(const Vector3D &coord) {
				return Vector2D(
						coord.x / quadratic_sum_sqrt(coord.y, coord.z),
						atan2(coord.y, coord.z));
			}

			static inline Vector3D proj2homo(const Vector2D &coord) {
				return Vector3D(coord.x, sin(coord.y), cos(coord.y));
			}
		};
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

