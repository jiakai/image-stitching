/*
 * $File: gain_compensator.cc
 * $Date: Wed May 08 20:35:05 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "stitcher_mat.hh"
#include "stitcher_impl.hh"
#include "filter/equalizer.hh"
#include "config.hh"

#include <cstdio>
#include <iostream>
using namespace std;
using namespace stitcher_impl;
using namespace stitcher_impl::gain_compensator_impl;

GainCompensator::GainCompensator():
	m_enable(config.get_int("GC.ENABLE", 1)),
	m_fit_term_scale(config.get_real_t_vector("GC.FIT_TERM_SCALE", {0.05, 0.03, 0.06})),
	m_sample_size(config.get_int("GC.SAMPLE_SIZE", 16)),
	m_entropy_nr_quantum(config.get_int("GC.ENTROPY_NR_QUANTUM", 16))
{
	m_fit_order = m_fit_term_scale.size();
	assert(m_fit_order >= 2);
}


vector<real_t> GainCompensator::solve_coeff(
		const ImageArray &input_image) const{

	vector<Equation> eqn_group;
	Matrix<MatchedBlock> matched_block;

	int tot_nr_overlap = 0;

	// build equation group
	for (auto &cur_match: m_matched_img) {
		auto &img0 = input_image[cur_match.v0],
			 &img1 = input_image[cur_match.v1];

		int img0_h = img0.height(), img0_w = img0.width();
		matched_block.set_dimension(
				img0_w / m_sample_size + 1, img0_h / m_sample_size + 1);
		matched_block.fill_memset(0);

		for (int i = 0; i < img0_h; i ++)
			for (int j = 0; j < img0_w; j ++) {
				Vector2D p1 = cur_match.trans.trans_normalized(
						real_t(i) / img0_h, real_t(j) / img0_w);
				if (p1.x >= 0 && p1.x < 1 && p1.y >= 0 && p1.y < 1) {
					auto &tgt = matched_block.at(
							i / m_sample_size, j / m_sample_size);
					tgt.u0_sum += img0.at(i, j);
					tgt.u1_sum += img1.get_linear_interpolation_sc(p1.x, p1.y);
					tgt.cnt ++;
				}
			}

		for (auto &mb: matched_block) {
			if (!mb.cnt)
				continue;

			tot_nr_overlap += mb.cnt;

			real_t sqrt_n = sqrt(real_t(mb.cnt));

			// pair match error
			{
				real_t u0_avg = mb.u0_sum / mb.cnt,
					   u1_avg = mb.u1_sum / mb.cnt;
				eqn_group.push_back(Equation());
				auto &eqn = eqn_group.back();

				auto add_to_eqn = [&eqn, this]
					(int eqn_idx, real_t val, real_t scale) {
						real_t p = 1;
						for (int i = 0; i < m_fit_order; i ++) {
							eqn.term.emplace_back(get_coeff_idx(eqn_idx, i), p * scale);
							p *= val;
						}
					};

				add_to_eqn(cur_match.v0, u0_avg, sqrt_n);
				add_to_eqn(cur_match.v1, u1_avg, -sqrt_n);
			}

			// coefficient variance error
			for (int i = 0; i < m_fit_order; i ++) {
				eqn_group.push_back(Equation());
				auto &eqn = eqn_group.back();

				real_t fac = sqrt(m_fit_term_scale[i]) * sqrt_n;

				eqn.c = (i == 1 ? -fac : 0);
				eqn.term.emplace_back(get_coeff_idx(cur_match.v0, i), fac);
			}
		}
	}

	return solve_eqn(eqn_group, input_image.size() * m_fit_order);
}

ImageArray GainCompensator::run(const ImageArray &input_image,
		const ImageArray &feature_image) const {

	if (input_image.size() == 1 || !m_enable)
		return input_image;

	assert(input_image.size() == feature_image.size());

	auto coeff_all = solve_coeff(input_image);
	real_t coeff[m_fit_order];

	ImageArray ret(input_image.size());
	for (size_t i = 0; i < input_image.size(); i ++) {
		ret[i] = input_image[i].copy();
		for (int j = 0; j < m_fit_order; j ++)
			coeff[j] = coeff_all[get_coeff_idx(i, j)];

		for (auto &t: ret[i]) {
			real_t p = 1, s = 0;
			for (int j = 0; j < m_fit_order; j ++) {
				s += p * coeff[j];
				p *= t;
			}
			t = s;
		}

	}

	size_t max_entropy_pos = 0;
	// find max_entropy_pos
	{
		real_t max_entropy = 0;
		for (size_t i = 0; i < input_image.size(); i ++) {
			real_t h = get_entropy(feature_image[i]);
			if (h > max_entropy) {
				max_entropy = h;
				max_entropy_pos = i;
			}
		}
	}
	log_printf("max_entropy_pos=%d", int(max_entropy_pos));

	TrainableEqualizerFilter equalizer;
	// train equalizer
	{
		const pixel_t *src = ret[max_entropy_pos].data(),
			  *dest = input_image[max_entropy_pos].data();
		int size = ret[max_entropy_pos].area();
		for (int i = 0; i < size; i ++)
			equalizer.add(src[i], dest[i]);
		equalizer.finish_add();
	}

	for (auto &i: ret)
		equalizer.run_modify(i);


	return ret;
}

real_t GainCompensator::get_entropy(const Image &img) const {
	int cnt[m_entropy_nr_quantum + 1];
	memset(cnt, 0, sizeof(int) * (m_entropy_nr_quantum + 1));
	for (auto f: img) {
		if (f < 0)
			f = 0;
		else if (f > 1)
			f = 1;
		cnt[ifloor(f * m_entropy_nr_quantum)] ++;
	}
	real_t h = 0;
	for (int c: cnt)
		if (c) 
			h += c * log(real_t(c));
	real_t n = img.area();
	h = log(n) - h / n;
	return h;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
