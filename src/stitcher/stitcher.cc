/*
 * $File: stitcher.cc
 * $Date: Wed May 01 13:54:27 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "stitcher_impl.hh"
#include "config.hh"


ImageStitcher::Impl::Impl(ImageStitcher &st):
	m_stitcher(st)
{
}


ImageStitcher::ImageStitcher():
	m_impl(new Impl(*this)),
	m_fit_nr_iter(config.get_int("STITCHER.FIT_NR_ITER", 1000)),
	m_fit_good_nr_match(config.get_int("STITCHER.FIT_GOOD_NR_MATCH", 15)),
	m_seam_width(config.get_int("STITCHER.SEAM_WIDTH", 15)),
	m_match_method(config.get_int("STITCHER.MATCH_METHOD", 0)),
	m_blend_method(config.get_int("STITCHER.BLEND_METHOD", 1)),
	m_output_image_max_area(config.get_int("STITCHER.RESULT_MAX_SIZE", 5000 * 2000)),
	m_output_image_max_size(config.get_int("STITCHER.RESULT_MAX_SIZE", 5000)),
	m_fit_error_threshold(config.get_double("STITCHER.FIT_ERROR_THRESHOLD",
				3e-3)),
	m_homocoord_threshold(config.get_double("STITCHER.HOMOCOORD_THRESHOLD",
			0.05)),
	m_proj_flat_minw(config.get_double("STITCHER.PROJ_FLAT_MIN_W", 0.6)),
	m_proj_flat_max_angle(config.get_double(
				"STITCHER.PROJ_FLAT_MAX_ANGLE", 45 / 180.0 * M_PI)),
	m_proj_cylin_min_agl(config.get_double(
				"STITCHER.PROJ_CYLIN_MIN_ANGLE", 15 / 180.0 * M_PI)),
	m_ba_conf_thresh(config.get_double(
				"STITCHER.BA_CONF_THRESHOLD", 0.01))
{
}

ImageStitcher::~ImageStitcher() {
	delete m_impl;
}

void ImageStitcher::init(const FeatureMatcherPtrArray &matcher,
		const ImageArray &feature_image) {
	m_init_done = true;
	m_impl->init(matcher, feature_image);
}

ImageArray ImageStitcher::stitch(const ImageArray &image) const {
	assert(m_init_done);
	return m_impl->stitch(image);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

