/*
 * $File: stitcher_mat.hh
 * $Date: Tue Apr 30 14:45:53 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief functions involving matrix manipulation
 */
#pragma once

#include "math.hh"
#include "vector.hh"
#include "image.hh"

#include <cstring>
#include <vector>


namespace stitcher_impl {
	struct MatchedPoint {
		real_t x0, y0, x1, y1;
		MatchedPoint(real_t x0_ = 0, real_t y0_ = 0,
				real_t x1_ = 0, real_t y1_ = 0):
			x0(x0_), y0(y0_), x1(x1_), y1(y1_)
		{}
	};
	using MatchedPointArray = std::vector<MatchedPoint>;

	class BundleAdjustor;
	class Homography {
		real_t mat[9]; /*! row majored order */

		void normalize();
		friend class BundleAdjustor;

		public:
			Homography() {
				memset(mat, 0, sizeof(mat));
			}

			Homography(const real_t *m) {
				memcpy(mat, m, sizeof(mat));
			}

			inline Vector2D trans_normalized(
					real_t x, real_t y, real_t z = 1) const;

			inline Vector2D trans_normalized(
					const Vector2D &v) const {
				return trans_normalized(v.x, v.y);
			}

			inline Vector2D trans_normalized(
					const Vector3D &v) const {
				return trans_normalized(v.x, v.y, v.z);
			}

			inline Vector3D trans(real_t x, real_t y, real_t z = 1) const;

			inline Vector3D trans(const Vector3D &v) const {
				return trans(v.x, v.y, v.z);
			}

			inline Vector3D trans(const Vector2D &v) const {
				return trans(v.x, v.y);
			}

			/*!
			 * \brief set to identity homography
			 */
			void set_as_identity();

			Homography operator * (const Homography &rhs) const;

			Homography get_inv() const;

			real_t min_w() const {
				if (mat[8] < EPS)
					return -1;
				return (min(mat[6], 0) + min(mat[7], 0) + mat[8]) / mat[8];
			}

			real_t get(int r, int c) const {
				return mat[r * 3 + c];
			}

			/*!
			 * \brief compute the homography from matched points to transform
			 *		(x0, y0) to (x1, y1)
			 */
			static Homography calc_from_match(const MatchedPointArray &match);

			/*!
			 * \brief compute a matrix to transform so that the points on
			 *		_plane_ could be projected to the y-z plane
			 */
			static Homography calc_trans_for_cylindrical_proj(
					const std::vector<Vector3D> &plane);
	};

	namespace gain_compensator_impl {

		/*!
		 * represent a term in the optimization target:
		 *		(sigma(term[i].coeff * x[term[i].idx]) + c)^2
		 */
		struct Equation {
			struct Term {
				int idx;
				real_t coeff;

				Term(int i, real_t c):
					idx(i), coeff(c)
				{}
			};
			std::vector<Term> term;
			real_t c = 0;
		};

		/*!
		 * \brief solve m_eqn and store solution in m_coeff
		 * note that this function is implemented in mat.cc
		 * \return total error
		 */
		std::vector<real_t> solve_eqn(const std::vector<Equation> &eqn_group,
				int nr_unknown);

	}

	/*!
	 * \brief compute A^(-1) * b
	 */
	std::vector<real_t> solve_linear_eqn(
			const Matrix<real_t> &A, const std::vector<real_t> &b);

}
using namespace stitcher_impl;

Vector2D Homography::trans_normalized(
		real_t x, real_t y, real_t z) const {

	Vector3D v(trans(x, y, z));
	if (v.z < EPS)
		return Vector2D::NaN();
	return Vector2D(v.x / v.z, v.y / v.z);
}

Vector3D Homography::trans(real_t x, real_t y, real_t z) const {
	return Vector3D(
			x * mat[0] + y * mat[1] + mat[2] * z,
			x * mat[3] + y * mat[4] + mat[5] * z,
			x * mat[6] + y * mat[7] + mat[8] * z);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

