/*
 * $File: fast_gaussian.cc
 * $Date: Sat Apr 27 17:31:10 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "filter/fast_gaussian.hh"
#include "config.hh"

#include <cmath>
#include <cstring>

FastGaussianFilter::FastGaussianFilter(real_t sigma):
	m_sigma(sigma),
	m_nr_iter(config.get_int("FASTGAUSS.NR_ITER", 4))
{
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

