/*
 * $File: gaussian.cc
 * $Date: Sat Apr 27 16:50:59 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "utils.hh"
#include "filter/gaussian.hh"

#include <cmath>
#include <cstring>

GaussianFilter::GaussianFilter(real_t sigma): m_sigma(sigma)
{
	assert(sigma >= 0);

	m_size = iceil(sigma * 3);
	m_weight_buf = create_auto_buf<real_t>(m_size * 2 + 1);
	m_weight = m_weight_buf.get() + m_size;
	real_t *weight = m_weight;

	weight[0] = 1;

	real_t exp_coeff = -1.0 / (sigma * sigma * 2),
		   wsum = weight[0];
	for (int i = 1; i <= m_size; i ++)
		wsum += (weight[i] = exp(i * i * exp_coeff)) * 2;

	real_t fac = 1.0 / wsum;
	weight[0] = fac;
	for (int i = 1; i <= m_size; i ++)
		weight[-i] = (weight[i] *= fac);
}

void GaussianFilter::run(const Image &img_in, pixel_t *img_out) {
	const real_t *weight = m_weight;
	int height = img_in.height(), width = img_in.width(), size = m_size;
	auto cur_line_mem = create_auto_buf<pixel_t>(
			size * 2 + max(width, height), true);
	pixel_t *cur_line = cur_line_mem.get() + size;

	// apply to columns
	for (int j = 0; j < width; j ++) {
		const pixel_t *src = img_in.data(0, j);
		for (int i = 0; i < height; i ++) {
			cur_line[i] = *src;
			src += width;
		}

		{
			pixel_t v0 = cur_line[0];
			for (int i = 1; i <= size; i ++)
				cur_line[-i] = v0;
			v0 = cur_line[height - 1];
			for (int i = 0; i < size; i ++)
				cur_line[height + i] = v0;
		}

		pixel_t *dest = img_out + j;
		for (int i = 0; i < height; i ++) {
			real_t tmp = 0;
			for (int k = -size; k <= size; k ++)
				tmp += weight[k] * cur_line[i + k];
			*dest = tmp;
			dest += width;
		}
	}

	// apply to rows
	for (int i = 0; i < height; i ++) {
		pixel_t *dest = img_out + i * width;
		memcpy(cur_line, dest, sizeof(cur_line[0]) * width);
		{
			pixel_t v0 = cur_line[0];
			for (int j = 1; j <= size; j ++)
				cur_line[-j] = v0;
			v0 = cur_line[width - 1];
			for (int j = 0; j < size; j ++)
				cur_line[width + j] = v0;
		}
		for (int j = 0; j < width; j ++) {
			real_t tmp = 0;
			for (int k = -size; k <= size; k ++)
				tmp += weight[k] * cur_line[j + k];
			*(dest ++) = tmp;
		}
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

