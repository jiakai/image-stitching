% $File: homography.tex
% $Date: Thu May 02 20:55:06 2013 +0800
% $Author: jiakai <jia.kai66@gmail.com>

\section{Homography}
\nocite{szeliski2006image}

\subsection{Perspective Projection of 3D Point onto Plane}
% f{{{
Let $\vec{P_0}$ be a point and $\vec{\alpha}, \vec{\beta}$ two vectors,
which together define a plane. Our task is, for an arbitrary point $\vec{P}$, 
find $x, y, k \in \real$ such that
\begin{equation}
	k\vec{P} = \vec{P_0} + x\vec{\alpha} + y\vec{\beta}
	\label{eqn:projection:def}
\end{equation}

Cross product on the right by $\vec{\beta}$ and
then dot product by $\vec{P}$ on \eqnref{projection:def}, we have
\begin{eqnarray*}
	k (\vec{P} \times \vec{\beta}) \cdot \vec{P} & = &
		(\vec{P_0} \times \vec{\beta}) \cdot \vec{P} +
		x (\vec{\alpha} \times \vec{\beta}) \cdot \vec{P} +
		y (\vec{\beta} \times \vec{\beta}) \cdot \vec{P} \\
	0 & = & (\vec{P_0}, \vec{\beta}, \vec{P}) + 
		x (\vec{\alpha}, \vec{\beta}, \vec{P})
\end{eqnarray*}
thus
\begin{equation}
	(\vec{\alpha}, \vec{\beta}, \vec{P}) x  = 
		(\vec{\beta}, \vec{P_0}, \vec{P})
	\label{eqn:projection:x}
\end{equation}
where $(\vec{\alpha}, \vec{\beta}, \vec{\gamma})$ is defined to be
$(\vec{\alpha} \times \vec{\beta}) \cdot \vec{\gamma}$.

Similarly we have
\begin{equation}
	(\vec{\alpha}, \vec{\beta}, \vec{P}) y =
		(\vec{P_0}, \vec{\alpha}, \vec{P})
	\label{eqn:projection:y}
\end{equation}

Let $\homo{x}$ be the homogeneous coordinate of the projection point and
\begin{equation}
	\mat{H} = \begin{matrix}{c}
		\trans{(\vec{\beta} \times \vec{P_0})} \\
		\trans{(\vec{P_0} \times \vec{\alpha})} \\
		\trans{(\vec{\alpha} \times \vec{\beta})}
	\end{matrix}
\end{equation}

Then we have
\begin{equation}
	\homo{x} \sim \mat{H}\vec{P} = \begin{colvec}
		wx \\
		wy \\
		w
	\end{colvec}
\end{equation}
where
\begin{equation}
	w=(\vec{\alpha}, \vec{\beta}, \vec{P})
	\label{eqn:projection:w}
\end{equation}
Note that the conditions $w > 0$, $w = 0$ and $w < 0$
separate all points according to their relative position to the focal plane.
% f}}}

\subsection{Transformation Between Views}
% f{{{
Let $\vec{P} = \trans{[x~y~z]}$ be an arbitrary point in 3D space,
$\homo{P} = \trans{[x~y~z~1]}$ its homogeneous coordinate,
and $\mat{H_0}$ its projection matrix onto a plane. Let
\begin{equation}
	\mat{H} = \begin{matrix}{cc}
		\mat{H_0} & \vec{0} \\
		\trans{\vec{0}} & 1
	\end{matrix}
\end{equation}
Then $\vec{P_p} = \mat{H}\vec{P} = \trans{[wx ~ wy ~ w ~ 1]}$
is its projection containing the depth information.

The transformation of projection coordinate of a point between views would
be easy,
if we knew the ``depth'' dimension of the projection point,
\begin{eqnarray}
	\homo{P'} & \sim & \mat{H'}\mat{E}\inv{\mat{H}}\vec{P_p} \nonumber \\
		& \sim & \mat{T}\trans{[wx ~ wy ~ w ~ 1]} \nonumber \\
		& \sim & \mat{T}\trans{[x ~ y ~ 1 ~ \dfrac{1}{w}]}
		\label{eqn:projection:P'}
\end{eqnarray}
where $\mat{H}$ and $\mat{H'}$ are the projection matrices of the two views
and $\mat{E}$ the transformation between their focal points.

Without knowledge about $w$,
the problem is still solvable under the assumption of the scene being planar.
That is, we assume $\vec{n}\cdot\vec{P} + c = 0$ always holds.
Let $\vec{\gamma_1} = \vec{\beta} \times \vec{P_0}$,
$\vec{\gamma_2} = \vec{P_0} \times \vec{\alpha}$
and $\vec{\gamma_3} = \vec{\alpha} \times \vec{\beta}$ in
\eqnref{projection:x}, \eqnref{projection:y} and \eqnref{projection:w}, then
$\vec{n}$ can be decomposed as
\begin{eqnarray}
	\vec{n} = k_1 \vec{\gamma_1} + k_2 \vec{\gamma_2} + k_3 \vec{\gamma_3}
\end{eqnarray}
substituting into $\vec{n}\cdot\vec{P} + c = 0$,
\begin{equation*}
	k_1 (\vec{\gamma_1} \cdot \vec{P}) +
	k_2 (\vec{\gamma_2} \cdot \vec{P}) +
	k_3 (\vec{\gamma_3} \cdot \vec{P}) + c = 0
\end{equation*}
that is,
\begin{equation*}
	k_1 w x + k_2 w y + k_3 w + c = 0
\end{equation*}
thus,
\begin{equation*}
	\begin{colvec}
		x \\ y \\ 1 \\ \dfrac{1}{w}
	\end{colvec} =
	\begin{matrix}{cccc}
		& & & \\
		& \mat{I} & & \vec{0} \\
		& & & \\
		-\dfrac{k_1}{c} & -\dfrac{k_2}{c} & -\dfrac{k_3}{c} & 0
	\end{matrix}
	\begin{colvec}
		x \\ y \\ 1 \\ 0
	\end{colvec} =
	\mat{A}
	\begin{colvec}
		x \\ y \\ 1 \\ 0
	\end{colvec}
\end{equation*}

Substituting into \eqnref{projection:P'}, we have
$\homo{P'} \sim \mat{T}\mat{A}\trans{[x~y~1~0]}$.
And we do not care about the depth dimension of $\homo{P'}$,
so we have the formula
\begin{equation}
	\begin{colvec}
		x' \\ y' \\ 1
	\end{colvec} \sim
	\mat{M}
	\begin{colvec}
		x \\ y \\ 1
	\end{colvec}
	\label{eqn:projection:finaltrans}
\end{equation}
where $\mat{M}$ is a 3 by 3 invertible matrix.

It should also be noted that if the transformation is pure rotation,
then the last column of $\mat{E}$ in \eqnref{projection:P'} is $\vec{0}$,
thus \eqnref{projection:finaltrans} still holds.

Therefore, under the assumption that either the user only rotates the
camera without shifting or the scene is flat, it is reasonable to
model transform between images using 8-parameter homographies.

% f}}}

% vim: filetype=tex foldmethod=marker foldmarker=f{{{,f}}}

